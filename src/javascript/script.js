$(document).ready(function () {

  $(window).resize(function () {
    if ($(window).width() <= 767) {
      $(function () {
        $('[data-toggle="popover"]').popover({
          placement: 'bottom',
        });
      })
    }
  });

  let openMenu = document.querySelector('.burger');
  let closeMenu = document.querySelector('.mobile-nav__exit');

  openMenu.onclick = function (e) {
    e.preventDefault();
    document.querySelector('#mobile-nav').classList.add('mobile-nav--is-active');
    document.querySelector('.document-wrapper').classList.add('over');
  };

  closeMenu.onclick = function (e) {
    e.preventDefault();
    document.querySelector('#mobile-nav').classList.remove('mobile-nav--is-active');
    document.querySelector('.document-wrapper').classList.remove('over');
  };

  // openMenu.onclick = function (e) {
  //   e.preventDefault();
  //   let mobileNav = document.querySelector('.mobile-nav');
  //   if ( mobileNav.classList.contains('mobile-nav--is-active') == false ) {
  //     mobileNav.classList.add('mobile-nav--is-active');
  //   } else {
  //     mobileNav.classList.remove('mobile-nav--is-active');
  //   };
  // };

  $('.special-offers__slider').owlCarousel({
    dots: false,
    margin: 30,
    nav: true,
    responsive: {
      0: {
        items: 1,
        navText: ['<div class="arrow-left--small"></div>', '<div class="arrow-right--small"></div>'],
      },
      768: {
        items: 1,
        navText: ['<div class="arrow-left"></div>', '<div class="arrow-right"></div>'],
      },
      1200: {
        items: 1,
        navText: ['<div class="arrow-left"></div>', '<div class="arrow-right--white"></div>'],
      }
    }
  });

  $('.testimonials__slider').owlCarousel({
    dots: false,
    responsive: {
      0: {
        items: 1,
        margin: 10,
        stagePadding: 30,
        loop: true,
      },
      768: {
        items: 2,
        margin: 20,
        stagePadding: 0,
        loop: false,
        nav: true,
        navText: ['<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
      },
      1200: {
        items: 3,
        margin: 20,
        stagePadding: 0,
        loop: true,
        nav: true,
        navText: ['<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
      }
    }
  });

  $('.projects__slider').owlCarousel({
    dots: false,
    center: true,
    smartSpeed: 500,
    responsive: {
      0: {
        items: 1,
        margin: -20,
        stagePadding: 50,
        loop: true,
      },
      768: {
        items: 3,
        margin: 0,
        stagePadding: 0,
        loop: true,
        nav: true,
        navText: ['<div class="arrow-left"></div>', '<div class="arrow-right"></div>'],
      },
      1200: {
        items: 3,
        margin: 0,
        stagePadding: 0,
        loop: true,
        nav: true,
        navText: ['<div class="arrow-left"></div>', '<div class="arrow-right"></div>'],
      }
    }
  });

});

var mapPoints = [
    [
      'Dynasty',
      59.987011,
      30.399723
    ],
  ];

function initMap() {
  var mapDiv = document.getElementById('map');
  var center = {lat: 59.987011, lng: 30.399723};
  var map = new google.maps.Map(mapDiv, {
    zoom: 13,
    center: center,
    scrollwheel: false,
  });

  setMapMarkers(map);

  map.addListener('resize', function() {
    map.panTo(center);
  });
  google.maps.event.addDomListener(window, 'resize', function(){
    google.maps.event.trigger(map, 'resize');
  });
}

function setMapMarkers(map) {
  var icon = {
    url: '../images/map-point.png',
    size: new google.maps.Size(40, 53),
    anchor: new google.maps.Point(20, 53)
  };

  for (var i = 0; i < mapPoints.length; i++) {
    var point = mapPoints[i];
    var marker = new google.maps.Marker({
      position: {lat: point[1], lng: point[2]},
      map: map,
      icon: icon,
      title: point[0],
      html: point[3],
    });
  }
}